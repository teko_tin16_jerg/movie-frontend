import { Component } from '@angular/core';
import { CounterService } from './counter.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  counter$: Observable<number>;

  constructor(private counterService: CounterService) {

      this.counter$ = this.counterService.counter$.pipe(
        tap(e=> console.log(e))
      );
  }
}
