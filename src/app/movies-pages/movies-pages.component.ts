import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { BackendService } from '../backend.service';
import {Movie} from '../models';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-movies-pages',
  templateUrl: './movies-pages.component.html',
  styleUrls: ['./movies-pages.component.css']
})
export class MoviesPagesComponent implements OnInit {

  movies: Movie[];

  constructor(
    private router: Router,
    private backend: BackendService,
    private counterService: CounterService) { }

  ngOnInit() {
    this.backend.getMovies().subscribe(movies => {
      this.movies = movies;
    });
  }

  goToMovieDetail(movieId: string) {
    console.log('received', movieId);
    this.counterService.count();
    this.router.navigate(['/movies', movieId]);
  }


}
