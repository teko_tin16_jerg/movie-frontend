import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';

import {Movie} from './models';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  constructor(private http: HttpClient) {}

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>('./assets/movies.json');
  }

  // Murks, weil wir vom json auslesen
  getMovie(movieID: string): Observable<Movie> {
    return this.getMovies().pipe(
      map(items => items.find(item => item.id === movieID))
    );
  }
}
