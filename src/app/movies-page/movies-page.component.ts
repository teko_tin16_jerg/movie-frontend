import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BackendService } from '../backend.service';
import { Movie } from '../models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-movies-page',
  templateUrl: './movies-page.component.html',
  styleUrls: ['./movies-page.component.css']
})
export class MoviesPageComponent implements OnInit {
  // Object-Based
  movie: Movie;

  // Observable-Based sollte bevorzugt werden
  movie$: Observable<Movie>;

  constructor(
    private router: ActivatedRoute,
    private backendService: BackendService
  ) {}

  ngOnInit() {
    console.log('Movie Page init...');

    // this.movieId = this.router.snapshot.params.movieId;
    this.backendService
      .getMovie(this.router.snapshot.params.movieId)
      .subscribe(movie => {
        this.movie = movie;
      });

    this.movie$ = this.backendService.getMovie(
      this.router.snapshot.params.movieId
    );
  }
}
