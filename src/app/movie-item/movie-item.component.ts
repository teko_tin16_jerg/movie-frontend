import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import {Movie} from '../models';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.css']
})
export class MovieItemComponent implements OnInit {

  @Input() movie: Movie;
  @Output() clicked = new EventEmitter<string>();
  //@Input() title: string;

  constructor() { }

  ngOnInit() {
  }

  onClicked(){
    console.log('clicked', this.movie.id);
    this.clicked.emit(this.movie.id);
  }

}
