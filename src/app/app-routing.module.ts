import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesPagesComponent } from './movies-pages/movies-pages.component';
import { MoviesPageComponent } from './movies-page/movies-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'movies',
    pathMatch: 'full'
  },
  {
    path: 'movies',
    component: MoviesPagesComponent
  },
  {
    path: 'movies/:movieId',
    component: MoviesPageComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
