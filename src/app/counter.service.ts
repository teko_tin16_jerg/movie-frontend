import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CounterService {
  private counterContainer = 0;

  private counterSubject = new BehaviorSubject<number>(this.counterContainer);

  constructor() {}

  get counter$(): Observable<number> {
    return this.counterSubject.asObservable();
  }

  count() {
    ++this.counterContainer;
    this.counterSubject.next(this.counterContainer);
  }
}
