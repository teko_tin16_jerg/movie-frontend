import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoviesPagesComponent } from './movies-pages/movies-pages.component';
import { MoviesPageComponent } from './movies-page/movies-page.component';
import { MovieItemComponent } from './movie-item/movie-item.component';


@NgModule({
  declarations: [
    AppComponent,
    MoviesPagesComponent,
    MoviesPageComponent,
    MovieItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
